package bowlinggame;

import bowlinggame.exceptions.InvalidNumberOfSingleThrowsException;
import bowlinggame.game.SingleBowlingGame;
import bowlinggame.game.frame.SingleFrame;
import bowlinggame.game.frame.singlethrow.SingleThrow;
import bowlinggame.helpers.CalculateScore;
import org.junit.Assert;
import org.junit.Test;

public class BowlingGameTests {

    @Test
    public void gutterGame() {
        SingleBowlingGame myGame = new SingleBowlingGame();
        SingleFrame frame1 = new SingleFrame();
        frame1.addSingleThrowToFrame(new SingleThrow("0"));
        frame1.addSingleThrowToFrame(new SingleThrow("0"));
        for (int i = 0; i < 10; i++) {
            myGame.addFrame(frame1);
        }
        CalculateScore score = new CalculateScore();
        Assert.assertEquals(0, score.calculateTotalScore(myGame));
    }

    @Test
    public void allSinglePinThrows() {
        SingleBowlingGame myGame = new SingleBowlingGame();
        SingleFrame frame1 = new SingleFrame();
        frame1.addSingleThrowToFrame(new SingleThrow("1"));
        frame1.addSingleThrowToFrame(new SingleThrow("1"));
        for (int i = 0; i < 10; i++) {
            myGame.addFrame(frame1);
        }
        CalculateScore score = new CalculateScore();
        Assert.assertEquals(20, score.calculateTotalScore(myGame));
    }

    @Test
    public void spareTest() {
        SingleBowlingGame myGame = new SingleBowlingGame();
        SingleFrame frame1 = new SingleFrame();
        frame1.addSingleThrowToFrame(new SingleThrow("1"));
        frame1.addSingleThrowToFrame(new SingleThrow("/"));
        SingleFrame frame2 = new SingleFrame();
        frame2.addSingleThrowToFrame(new SingleThrow("3"));
        frame2.addSingleThrowToFrame(new SingleThrow("0"));
        myGame.addFrame(frame1);
        myGame.addFrame(frame2);
        CalculateScore score = new CalculateScore();
        Assert.assertEquals(16, score.calculateTotalScore(myGame));
    }

    @Test
    public void strikeTest() {
        SingleBowlingGame myGame = new SingleBowlingGame();
        SingleFrame frame1 = new SingleFrame();
        frame1.addSingleThrowToFrame(new SingleThrow("X"));
        SingleFrame frame2 = new SingleFrame();
        frame2.addSingleThrowToFrame(new SingleThrow("3"));
        frame2.addSingleThrowToFrame(new SingleThrow("4"));
        myGame.addFrame(frame1);
        myGame.addFrame(frame2);
        CalculateScore score = new CalculateScore();
        Assert.assertEquals(24, score.calculateTotalScore(myGame));
    }

    @Test
    public void perfectGame() {
        SingleBowlingGame myGame = new SingleBowlingGame();
        SingleFrame frame1 = new SingleFrame();
        frame1.addSingleThrowToFrame(new SingleThrow("X"));
        SingleFrame frame2 = new SingleFrame();
        frame2.addSingleThrowToFrame(new SingleThrow("X"));
        frame2.addSingleThrowToFrame(new SingleThrow("X"));
        frame2.addSingleThrowToFrame(new SingleThrow("X"));
        for (int i = 0; i < 9; i++) {
            myGame.addFrame(frame1);
        }
        myGame.addFrame(frame2);
        CalculateScore score = new CalculateScore();
        Assert.assertEquals(300, score.calculateTotalScore(myGame));
    }

    @Test
    public void invalidNumberOfSingleThrowsExceptionInvocation(){
        SingleBowlingGame myGame = new SingleBowlingGame();
        SingleFrame frame1 = new SingleFrame();
        frame1.addSingleThrowToFrame(new SingleThrow("1"));
        frame1.addSingleThrowToFrame(new SingleThrow("3"));
        frame1.addSingleThrowToFrame(new SingleThrow("8"));
         try {
             myGame.addFrame(frame1);
         } catch (InvalidNumberOfSingleThrowsException e){
             Assert.assertEquals(e.getMessage(), new InvalidNumberOfSingleThrowsException().getMessage());
         }
    }

    @Test
    public void singleThrowPointMoreThenUpperBoundary(){
        SingleBowlingGame myGame = new SingleBowlingGame();
        SingleFrame frame1 = new SingleFrame();
        frame1.addSingleThrowToFrame(new SingleThrow("11"));
        frame1.addSingleThrowToFrame(new SingleThrow("2"));
        myGame.addFrame(frame1);
        CalculateScore score = new CalculateScore();
        try {
            score.calculateTotalScore(myGame);
        } catch (NumberFormatException e){
            Assert.assertEquals(e.getMessage(), new NumberFormatException(myGame.getAllSingleFrames().get(0).getSingleThrowsInFrame().get(0).getNumberOfPins() + " is an invalid score input").getMessage());
        }
    }

    @Test
    public void singleThrowPointLessThenLowerBoundary(){
        SingleBowlingGame myGame = new SingleBowlingGame();
        SingleFrame frame1 = new SingleFrame();
        frame1.addSingleThrowToFrame(new SingleThrow("-1"));
        frame1.addSingleThrowToFrame(new SingleThrow("2"));
        myGame.addFrame(frame1);
        CalculateScore score = new CalculateScore();
        try {
            score.calculateTotalScore(myGame);
        } catch (NumberFormatException e){
            Assert.assertEquals(e.getMessage(), new NumberFormatException(myGame.getAllSingleFrames().get(0).getSingleThrowsInFrame().get(0).getNumberOfPins() + " is an invalid score input").getMessage());
        }
    }
}
