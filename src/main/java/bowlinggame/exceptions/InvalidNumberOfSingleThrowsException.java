package bowlinggame.exceptions;

public class InvalidNumberOfSingleThrowsException extends RuntimeException {

    public InvalidNumberOfSingleThrowsException(){
        super("Invalid number of single throws for this frame");
    }
}
