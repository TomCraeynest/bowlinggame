package bowlinggame.helpers;

import bowlinggame.convertor.ConvertScoreToInt;
import bowlinggame.game.SingleBowlingGame;
import bowlinggame.game.frame.SingleFrame;
import bowlinggame.game.frame.singlethrow.SingleThrow;

import java.util.List;

public class CalculateScore {

    /**
     * Private method to verify if the played frame is a spare frame
     */

    private boolean isSpare(SingleFrame frame) {
        return frame.getSingleThrowsInFrame().get(1).getNumberOfPins().equals("/");
    }

    /**
     * Private method to verify if played frame is a Strike
     */

    private boolean isStrike(SingleFrame frame) {
        return frame.getSingleThrowsInFrame().get(0).getNumberOfPins().equalsIgnoreCase("x");
    }

    /**
     * Method that will calculate the score of a complete bowling game
     */

    public int calculateTotalScore(SingleBowlingGame bowlingGame) {
        int score = 0;
        List<SingleFrame> framesPlayed = bowlingGame.getAllSingleFrames();
        for (SingleFrame frame : framesPlayed) {
            if (isStrike(frame)) {
                int index = getFrameIndex(framesPlayed, frame);
                int upperIndex;
                upperIndex = (index + 3 > framesPlayed.size() - 1) ? framesPlayed.size() : index + 3;
                if (index == 9) {
                    score += calculateFrameScore(frame);
                } else {
                    score += calculateBonusForStrike(framesPlayed.subList(index + 1, upperIndex));
                }
            } else if (isSpare(frame)) {
                int index = getFrameIndex(framesPlayed, frame);
                score += calculateBonusForSpare(framesPlayed.subList(index + 1, index + 2));
            } else {
                score += calculateFrameScore(frame);
            }
        }
        return score;
    }

    private int getFrameIndex(List<SingleFrame> framesPlayed, SingleFrame frame) {
        return framesPlayed.indexOf(frame);
    }

    /**
     * Method that will calculate the bonus score of a strike frame
     */

    private int calculateBonusForStrike(List<SingleFrame> strikeFramesSubList) {
        int bonus = 10;
        for (SingleFrame currentFrame : strikeFramesSubList) {
            if (isStrike(currentFrame)) {
                bonus += 10;
            } else if (isSpare(currentFrame)) {
                bonus += 10;
            } else {
                bonus += calculateFrameScore(currentFrame);
            }
        }
        return bonus;
    }

    /**
     * Method that will calculate the bonus score of a spare  frame
     */

    private int calculateBonusForSpare(List<SingleFrame> spareFramesSublist) {
        int bonus = 10;
        if (isStrike(spareFramesSublist.get(0))) {
            bonus += 10;
            if (isStrike(spareFramesSublist.get(1))) {
                bonus += 10;
            } else {
                bonus += ConvertScoreToInt.convertStringScoreToInt(spareFramesSublist.get(1).getSingleThrowsInFrame().get(0).getNumberOfPins());
            }
        } else if (isSpare(spareFramesSublist.get(0))) {
            bonus += 10;
        } else {
            bonus += ConvertScoreToInt.convertStringScoreToInt(spareFramesSublist.get(0).getSingleThrowsInFrame().get(0).getNumberOfPins());
        }
        return bonus;
    }

    /**
     * Method that will calculate the score of a frame
     */

    private int calculateFrameScore(SingleFrame frame) {
        int frameScore = 0;
        for (SingleThrow myThrow : frame.getSingleThrowsInFrame()) {
            frameScore += ConvertScoreToInt.convertStringScoreToInt(myThrow.getNumberOfPins());
        }
        return frameScore;
    }
}
