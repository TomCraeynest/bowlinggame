package bowlinggame.convertor;

public class ConvertScoreToInt {
    public static int convertStringScoreToInt(String score){
        switch (score){
            case "X": return 10;
            case "/": return -1;
            default:
                try {
                    int myScore =  Integer.parseInt(score);
                    if(myScore > 10 || myScore < 0){
                        throw new NumberFormatException( score + " is an invalid score input");
                    }
                    return myScore;
                } catch (NumberFormatException e){
                    throw new NumberFormatException( score + " is an invalid score input");
                }

        }
    }
}
