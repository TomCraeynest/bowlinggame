package bowlinggame.game;

import bowlinggame.exceptions.InvalidNumberOfSingleThrowsException;
import bowlinggame.game.frame.SingleFrame;

import java.util.ArrayList;
import java.util.List;

public class SingleBowlingGame {
    private final List<SingleFrame> frames;

    public SingleBowlingGame() {
        this.frames = new ArrayList<>();
    }

    public List<SingleFrame> getAllSingleFrames() {
        return frames;
    }

    public void addFrame(SingleFrame frame) {
        //Verify correctness of frames
        if ((frames.size() < 9 && frame.getFrameSize(frame) > 2) || (frames.size() == 9 && frame.getFrameSize(frame) > 3)) {
            throw new InvalidNumberOfSingleThrowsException();
        }
        this.frames.add(frame);
    }
}
