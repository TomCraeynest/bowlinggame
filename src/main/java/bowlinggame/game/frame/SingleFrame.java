package bowlinggame.game.frame;

import bowlinggame.game.frame.singlethrow.SingleThrow;

import java.util.ArrayList;
import java.util.List;

public class SingleFrame {
    private final List<SingleThrow> singleThrows;

    public SingleFrame(){
        this.singleThrows = new ArrayList<>();
    }

    public List<SingleThrow> getSingleThrowsInFrame() {
        return singleThrows;
    }

    public void addSingleThrowToFrame(SingleThrow singleThrow){
        this.singleThrows.add(singleThrow);
    }

    public int getFrameSize(SingleFrame currentFrame){
        return currentFrame.getSingleThrowsInFrame().size();
    }
}
