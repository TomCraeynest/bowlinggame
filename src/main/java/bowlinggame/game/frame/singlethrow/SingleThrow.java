package bowlinggame.game.frame.singlethrow;

public class SingleThrow {

    private final String numberOfPins;

    public SingleThrow(String numberOfPins) {
        this.numberOfPins = numberOfPins;
    }

    public String getNumberOfPins() {
        return numberOfPins;
    }

}
